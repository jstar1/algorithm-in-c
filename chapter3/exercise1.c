#include <stdio.h>

/* Greatest common divisor
 * using Euclidean Algorithm
 */

int gcd(int a, int b);
void print_array(int (*ar)[10]);

int main()
{
    int array[4][10]; 
    int i, j, c;

    //Initialize array with 1/true
    for (i = 1; i <= 3; ++i){
        for (j = 1; j <= 9; ++j) {
            array[i][j] = 1;
        };
    };
    
    // check work
    printf("This is initlialized.\n");
    print_array(array); 
    
    // set 0 if array is not prime
    for ( i = 1; i <= 3; ++i) {
        for ( j = 1; j <= 9; ++j) {
            if (c = gcd(i,j) > 1) {
                array[i][j] = 0;
            }
        };
    };                

    //Check work
    printf("This is gcd.\n");
    print_array(array);
}

// Implementation of Euclidean Algorithm
int gcd(int a, int b) 
{
    if(a == 1 && b == 1) return 2;
    int t;
    while (b != 0) {
        t = b;
        b = a % b;
        a = t;
    }
    return a;
}

// Print array
void print_array(int (*array)[10])
{
    int i, j = 0;
    for (i = 1; i <= 3; ++i){
        for (j = 1; j <= 9; ++j) {
            printf("i: %d, j: %d, gcd: %d\n", i, j, array[i][j]);
        };
    };
}

