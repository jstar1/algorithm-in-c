#include <stdio.h>
#include <stdlib.h>

struct node {
    int key;
    struct node* next;
};

// Global
struct node* head = NULL;
struct node* tail = NULL;

// Prototype
void movenexttofront(struct node *t);
void freeList();

int main()
{
    int i, N, M = 0;
    struct node* t;

    printf("Number of nodes.");
    scanf("%d", &N);
    printf("Which node should t point to.");
    scanf("%d", &M);
    
    // initialitaile head
    t = (struct node*) malloc(sizeof *t);
    t->key = 1;
    head = t;
    
    // initialitaile LL 
    for (i = 2; i <= N; ++i) {
        t->next = (struct node *) malloc(sizeof *t);
        t = t->next;
        t->key = i;  
    }
    // Set tail pointer Reset t
    tail = t;
    tail->next = tail;
    t = head;
    
    // Set t
    struct node* current;
    current = head;
    while (current) {
        if (current->key == M) { 
            t = current; 
            break; 
        }
        current = current->next;
    }
    
    // move next to front  
    movenexttofront(t);
    
    // test
    current = head;
    while (current) {
        printf("%d\n", current->key);
        if (current == tail) {
            break;
        }
        current = current->next;
    } 
    
    // free memory
    freeList();

    return 0;
}

void movenexttofront(struct node *t)
{
    struct node* current = t;
    while (current) {
        // Case 1
        if (current == tail) {
            printf("Case 1\n");
            printf("No changes\n");
            printf("Head key:%d\n",head->key);
            break;
        }
        // Case 2
        if (current == t && current->next != tail) {
            printf("Case 2\n");
            current = current->next;
            t->next = current->next;
            current->next = head;
            head = current;
            printf("Head key:%d\n",head->key);
            break;
        }
        // Case 3
        if (current == t && current->next == tail) {
            printf("Case 3\n");
            current = tail;
            tail = t;
            current->next = head;
            tail->next = tail;
            head = current;
            printf("Head key:%d\n",head->key);
            break;
        } 
        current = current->next;
    }
}

void freeList()
{
    struct node* tmp;
    tail->next = NULL;
    while (head){
        printf("This node is on the chopping block:%d\n",head->key);  
        tmp = head;
        head = head->next;
        tmp->key = 0;
        free(tmp);
    }
}
