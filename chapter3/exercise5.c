#include <stdio.h>
#include <stdlib.h>

/* Program that inserts and deletes
   nodes in a doubly linked list */

typedef struct Node Node;

struct Node {
    int key;
    Node* next;
    Node* previous;
};

// Prototypes
Node* create_head();
Node* create_list(Node* node, int S);
Node* insert_node(Node* node, int k);
Node* delete_node(Node* node, int k);
void free_list(Node* node);
void display_list(Node* node);

int main(int argc, char* argv[])
{

    long n;
    int l, M, d = 0;

    n = strtol(argv[argc-1], NULL, 10);
    l = n;

    // init head
    Node* head = create_head(); 
    // init list
    head = create_list(head, l); 
    display_list(head);

    printf("What node do you want add at? "); 
    scanf("%d", &M);
    head = insert_node(head, M);
    printf("This is after the insert at\n");
    display_list(head);

    printf("Which node do you want to delete?\n");
    scanf("%d", &d);
    head = delete_node(head, d); 
    printf("This is after the deletetion\n");
    display_list(head);
    
    free_list(head);

    return EXIT_SUCCESS;
}

Node* create_head()
{
    // allocate node struct 
    Node* node = malloc(sizeof (Node));
    if (!node)
        return NULL; 
    node->key = 1;
    node->next = NULL;
    node->previous = NULL; 
    return node;
}

Node* create_list(Node* node, int N)
{
    Node* head = node;
    Node* c = head;

    for (int i = 2; i <= N; ++i) {
        c->next = malloc(sizeof (Node));
        c->next->previous = c; 
        c = c->next;
        c->key = i;
    } 
    
    return head;
}

void display_list(Node* node)
{
    Node* c = node;
    while(c) {
        printf("This is the current key:%d\n", c->key);
        if(c->previous) printf("This is the previous:%d\n", c->previous->key);
        c = c->next;
    }
}

void free_list(Node* node)
{
    Node* head = node;
    Node* tmp;
    while(head){
        tmp = head;
        head = head->next; 
        tmp->key = 0;
        free(tmp);
    } 
}

Node* insert_node(Node* node, int k)
{
    Node* tmp = malloc(sizeof (Node)); 
    tmp->key = 0;
    Node* head = node;
    Node* c = head;

    while(c) {
        if(c->key == k) {
            if(!c->previous) {
                c->previous = tmp;
                tmp->next = c;
                tmp->previous = NULL;
                head = tmp; 
                return head;
            }
            if(!c->next){
                c->next = tmp;
                tmp->previous = c;
                tmp->next = NULL;
                return head;
            }
            tmp->next = c->next;
            tmp->previous = c;
            c->next->previous = tmp;
            c->next = tmp; 
            return head;
        }
        c = c->next;
    }
    return head;
}

Node *delete_node(Node* node, int d)
{
    Node* head = node;
    Node* tmp = head; 
    while(tmp) {
        if(tmp->key == d) {
            if(!tmp->previous) {
                head = head->next;
                tmp->key = 0;
                free(tmp);
                head->previous = NULL;   
                return head;
            }
            if(!tmp->next) {
                Node* c = tmp->previous;
                tmp->key = 0;
                free(tmp);
                c->next = NULL; 
                return head;
            }
            tmp->previous->next = tmp->next;
            tmp->next->previous = tmp->previous;
            tmp->key = 0;
            tmp->previous = NULL;
            tmp->next = NULL;
            free(tmp);
            return head;
        }
        tmp = tmp->next;
    }
    return head;
}






